/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.test.dao;

import org.apache.ibatis.annotations.Mapper;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.test.entity.TestData;

/**
 * 单表生成DAO接口
 * @author ThinkGem
 * @version 2015-04-06
 */
@Mapper
public interface TestDataDao extends CrudDao<TestData> {
	
}