/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.gen.dao;

import org.apache.ibatis.annotations.Mapper;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.modules.gen.entity.GenTemplate;
/**
 * 代码模板DAO接口
 * @author ThinkGem
 * @version 2013-10-15
 */
@Mapper
public interface GenTemplateDao extends CrudDao<GenTemplate> {
	
}
