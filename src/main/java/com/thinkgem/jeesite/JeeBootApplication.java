package com.thinkgem.jeesite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@EnableCaching
@SpringBootApplication(scanBasePackages = {"org.activiti.rest.editor","me.kafeitu"},exclude={org.activiti.spring.boot.SecurityAutoConfiguration.class,org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@ServletComponentScan(basePackages = "com.thinkgem.jeesite")
@ComponentScan(value = "com.thinkgem.jeesite", lazyInit = true)
public class JeeBootApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(JeeBootApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(JeeBootApplication.class, args);
	}
}
